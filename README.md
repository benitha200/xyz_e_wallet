XYZ E_Wallet 

This is the system that will help XYZ company in implementing electronic wallet system
where it will:


 "What system can do! (Demo) "

>allow users to create an account , and automatically get wallet with bonus of 1000
>send money from their wallet and transaction fee is charged according to the money sent
>when the user login get the information of the balance on his/her account and also transactions
  that have been made(money that was sent or received)
>There is security of the user wallet because to access it you have to be logged in( by "username" and "password" )
>The user alo can rechage( by adding money to his/her wallet balance)


"Codebase Walkthrough"

I have built this system using Yii2 framework, mysql database
I approched the problem by thinking how XYZ Company can implement this E-wallet according to the way they want it to be("Their predictions")

>I started by creating database which has three tables(which are : "users","Wallet","Wallet_transactions")
    >"users" to record all the users of the system
    >"wallet" to keep track of the wallets and their balance
    >"wallet_transactions" to record how transactions made

>User start by creating  sign up( if you don't have an account) and then login
>User get to a page where she/he can get the information about the account 
    >for the first time he/she gets bonus of 1000
    >the account balance is displayed and transactions that have been made is displayed(if any) 


"Why I did this?"

I approached this problem in this way because it have very high security by having login system,
The Framework that I used is very fast (It doesn't take so much time to load), It is user friendly (Easier to use)